$(document).ready(function() {

    if($('.search-result').hasClass('first')) {
        // web pertama kali di load, munculkan hasil search harry potter
        $(this).removeClass('first');
        $('.search-result').append('<div class="no-result">Searching...</div>');
        $.ajax({
            url : "/story8/data?q=harry potter",
            success : function(data) {
                var items = data.items;
                $('.search-result').empty();
                $('.info-story8').addClass('info-active');
                $('.info-1').addClass('info-active');
                $('.search-result').append(
                    "<table class='story8-table table-responsive'>"+
                    "<th>No</th><th>Title/Category</th><th>Author(s)</th><th>Published Date</th><th>Cover Image</th>"+
                    "</table>"
                );
                for(var i = 0; i < items.length; i++) {
                    judul = items[i].volumeInfo.title;
                    link = items[i].volumeInfo.previewLink;
                    cate = items[i].volumeInfo.categories;
                    author = items[i].volumeInfo.authors;
                    date = items[i].volumeInfo.publishedDate;
                    defaultImg = 'https://kknd26.ru/images/no_photo.png';
                    image = items[i].volumeInfo.imageLinks ? items[i].volumeInfo.imageLinks.smallThumbnail : defaultImg;
                    j = i+1;
                    $('.story8-table').append(
                        '<tr><td class = "index">' + j + 
                        '</td><td class="title"><a class="title-a" target="_blank" href=' + link + '>' + judul + 
                        '</a><br><label class="cate-b">' + cate + '</label></td><td class="auth">' + author + '</td><td class="date">' + date + 
                        '</td><td class="cover"><img class="img-cover" src=' + image + '></td></tr>' 
                    );
                }
            }
        })
    }

    $(".checkbox-search").click(function() {
        // pilih search type, 1&2 saat bukan default, 3 saat peralihan dari dafault ke non-default
        if(!$('#search-type-t').hasClass('active') && !$('.label').hasClass('label-nonactive')) {
            $('#search-type-t').addClass("active");
            $('#search-type-a').removeClass("active");
            $('#search-key').keyup();
        } else if (!$('#search-type-a').hasClass('active') && !$('.label').hasClass('label-nonactive')) {
            $('#search-type-t').removeClass("active");
            $('#search-type-a').addClass("active");
            $('#search-key').keyup();
        } else {
            $('.label').removeClass('label-nonactive');
            $('#search-type-t').addClass("active");
            $('#search-type-a').removeClass("active");
            $('.checkbox-search').prop('checked', false);
            $('.button-story8').addClass('button-nonactive');
            $('#search-key').keyup();
        }
    });

    $(".button-story8").click(function() {
        // pilih default search type
        if($('.button-story8').hasClass('button-nonactive')) {
            $('.button-story8').removeClass('button-nonactive');
            $('.label').addClass('label-nonactive');
            $('#search-type-t').removeClass("active");
            $('#search-type-a').removeClass("active");
            $('.checkbox-search').prop('checked', false);
            $('#search-key').keyup();
        }
    });

    $('input[type=search]').on('search', function () {
        // search logic here
        // this function will be executed on click of X (clear button), empty searchbox
        $('.search-result').empty();
        $('.info-story8').removeClass('info-active');
        $('.info-1').removeClass('info-active');
        $('.search-result').append('<div class="no-result">Waiting for you to type above⏳</div>');
    });

    $("#search-key").keyup(function() {
        // tiap dapat input keyword dari user
        var keyword = $(this).val();
        var urls;
        
        // depends on search type 
        if($('#search-type-a').hasClass('active')) {
            urls = "/story8/data?q=inauthor:" + keyword;
        } else if($('#search-type-t').hasClass('active')) {
            urls = "/story8/data?q=intitle:" + keyword;
        } else {
            urls = "/story8/data?q=" + keyword;
        }

        if(keyword != "") {
            // kalau user sudah mengetikkan keyword
            $('.search-result').empty();
            $('.info-story8').removeClass('info-active');
            $('.info-1').removeClass('info-active');
            $('.search-result').append('<div class="no-result">Searching...</div>');
            $.ajax({
                url : urls,
                success : function(data) {
                    $('.search-result').empty();
                    $('.info-story8').addClass('info-active');
                    $('.info-1').addClass('info-active');
                    if(data.totalItems > 0) {
                        var items = data.items;
                        
                        if($('.search-result').html()==''){
                            // ada result dari api
                            console.log(urls);
                            $('.search-result').append(
                                "<table class='story8-table table-responsive'>"+
                                "<th>No</th><th>Title/Category</th><th>Author(s)</th><th>Published Date</th><th>Cover Image</th>"+
                                "</table>"
                            );
                            for(var i = 0; i < items.length; i++) {
                                judul = items[i].volumeInfo.title;
                                link = items[i].volumeInfo.previewLink;
                                cate = items[i].volumeInfo.categories;
                                author = items[i].volumeInfo.authors;
                                date = items[i].volumeInfo.publishedDate;
                                defaultImg = 'https://kknd26.ru/images/no_photo.png';
                                image = items[i].volumeInfo.imageLinks ? items[i].volumeInfo.imageLinks.smallThumbnail : defaultImg;
                                j = i+1;
                                $('.story8-table').append(
                                    '<tr><td class = "index">' + j + 
                                    '</td><td class="title"><a class="title-a" target="_blank" href=' + link + '>' + judul + 
                                    '</a><br><label class="cate-b">' + cate + '</label></td><td class="auth">' + author + '</td><td class="date">' + date + 
                                    '</td><td class="cover"><img class="img-cover" src=' + image + '></td></tr>' 
                                );
                            }
                        }
                    } else {
                        // ternyata di api nya tidak ada result
                        $('.info-story8').removeClass('info-active');
                        $('.info-1').removeClass('info-active');
                        $('.search-result').append('<div class="no-result">No books found. Find something else🔍</div>');
                    }
                },
            })
        } else {
            // searchbox kosong
            $('.search-result').empty();
            $('.info-story8').removeClass('info-active');
            $('.info-1').removeClass('info-active');
            $('.search-result').append('<div class="no-result">Find your fav books here📚</div>');
        }
    });
});