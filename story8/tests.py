from django.test import TestCase, Client
from django.urls import resolve
from .views import *

# Create your tests here.

class Story8Test(TestCase):

    def test_url_story8_using_story8_func(self):
        response = Client().get('/story8/')
        found = resolve('/story8/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, story8)

    def test_story8_html_response(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'main/story8.html')

    def test_json_data(self):
        response = Client().get('/story8/data/?q=apayangceritanyagaada/')
        self.assertJSONEqual(response.content.decode("utf-8"), {"kind": "books#volumes", "totalItems": 0})