from django.db import models

# Create your models here.

class Orang(models.Model):
    name = models.CharField(max_length=25, null=True)

    def __str__(self):
        return self.name

class Kegiatan(models.Model):
    event = models.CharField(max_length=100, null=True)
    participants = models.ManyToManyField(Orang, blank=True)
    
    def __str__(self):
        return self.event

