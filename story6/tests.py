from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from .models import Kegiatan, Orang
from .forms import KegiatanForm, OrangForm

class Testing(TestCase):
    def setUp(self):
    #     self.client = Client()
    #     self.story6_url = reverse('story6')
        self.updateEvent_url = reverse('story6:updateEvent', args=['1'])
        self.removeParticipant_url = reverse('story6:removeParticipant', args=['1'])
        self.removeEvent_url = reverse('story6:removeEvent', args=['1'])
        self.orang1 = Orang.objects.create(name='nisa')
        self.kegiatan1 = Kegiatan.objects.create(event='nobar')

    def test_url_story6_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_matching_story6_view(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'main/story6.html', 'base.html')

    def test_url_addPerson_exist(self):
        response = Client().get('/story6/addPerson/')
        self.assertEqual(response.status_code, 200)

    def test_matching_addPerson_view(self):
        response = Client().get('/story6/addPerson/')
        self.assertTemplateUsed(response, 'main/addPerson.html', 'base.html')

    def test_url_addEvent_exist(self):
        response = Client().get('/story6/addEvent/')
        self.assertEqual(response.status_code, 200)

    def test_matching_addEvent_view(self):
        response = Client().get('/story6/addEvent/')
        self.assertTemplateUsed(response, 'main/addEvent.html', 'base.html')

    def test_apakah_model_orang_ada(self):
        # Orang.objects.create(name="nisa")
        hitung_berapa_orangnya = Orang.objects.all().count()
        self.assertEquals(hitung_berapa_orangnya, 1)

    def test_nama_model_orang(self):
        namaOrang = Orang.objects.create(name='nisa')
        self.assertEqual(str(namaOrang), 'nisa')

    def test_halaman_hasil_sudah_simpan_nama_orang(self):
        Orang.objects.create(name='nisa')

        response = Client().post('/story6/', {'name' : 'nisa'})
        self.assertEqual(response.status_code, 200)

    def test_nama_model_kegiatan(self):
        namaKegiatan = Kegiatan.objects.create(event='nobar')
        self.assertEqual(str(namaKegiatan), 'nobar')

    def test_halaman_hasil_sudah_simpan_nama_kegiatan(self):
        response = Client().post(self.updateEvent_url, {'event' : 'nobar'})
        self.assertEqual(response.status_code, 302)

    def test_valid_add_person(self):
        response = Client().post('/story6/addPerson/', data={'name' : 'nisa'})
        self.assertEqual(response["Location"], '/story6')

    def test_valid_add_event(self):
        response = Client().post('/story6/addEvent/', data={'event' : 'nobar'})
        self.assertEqual(response["Location"], '/story6')


    def test_url_updateEvent_exist(self):
        response = Client().get(self.updateEvent_url)
        self.assertEqual(response.status_code, 200)

    def test_url_removePerson_exist(self):
        response = Client().get(self.removeParticipant_url)
        self.assertEqual(response.status_code, 200)

    def test_matching_removePerson_view(self):
        response = Client().get(self.removeParticipant_url)
        self.assertTemplateUsed(response, 'main/removePar.html', 'base.html')

    def test_hapus_person(self):
        response = Client().post(self.removeParticipant_url)
        berapa_orang = Orang.objects.all().count()
        self.assertEquals(berapa_orang, 0)

    def test_url_removeEvent_exist(self):
        response = Client().get(self.removeEvent_url)
        self.assertEqual(response.status_code, 200)

    def test_matching_removeEvent_view(self):
        response = Client().get(self.removeEvent_url)
        self.assertTemplateUsed(response, 'main/removeEv.html', 'base.html')

    def test_hapus_kegiatan(self):
        response = Client().post(self.removeEvent_url)
        berapa_kegiatan = Kegiatan.objects.all().count()
        self.assertEquals(berapa_kegiatan, 0)