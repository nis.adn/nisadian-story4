from django.shortcuts import render, redirect
from .models import *
from .forms import KegiatanForm, OrangForm

def story6(request):
    kegiatan = Kegiatan.objects.all()
    orang = Orang.objects.all()

    context = {'kegiatan' : kegiatan, 'orang' : orang}
    return render(request, 'main/story6.html', context)

def addPerson(request):
    orang = Orang.objects.all()
    form = OrangForm()
    if request.method == 'POST':
        form = OrangForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/story6')

    context ={'form' : form, 'orang':orang}

    return render(request, 'main/addPerson.html', context)
    
def addEvent(request):
    kegiatan = Kegiatan.objects.all()
    form = KegiatanForm()
    if request.method == 'POST':
        form = KegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/story6')

    context ={'form' : form, 'kegiatan' : kegiatan, 'query' : 'add event'}

    return render(request, 'main/addEvent.html', context)

def updateEvent(request, pk):
    kegiatans = Kegiatan.objects.all()
    kegiatan = Kegiatan.objects.get(id=pk)
    form = KegiatanForm(instance=kegiatan)

    if request.method == 'POST':
        form = KegiatanForm(request.POST, instance=kegiatan)
        if form.is_valid():
            form.save()
            return redirect('/story6')

    context = {'form' : form, 'kegiatan':kegiatan, 'query':'update event'}
    return render(request, 'main/addEvent.html', context)

def removeEvent(request, pk):
    kegiatan = Kegiatan.objects.get(id=pk)
    context = {'item' : kegiatan, 'query' : 'Event'}

    if request.method == 'POST':
        kegiatan.delete()
        return redirect('/story6')

    return render(request, 'main/removeEv.html', context)

def removeParticipant(request, pk):
    orang = Orang.objects.get(id=pk)
    context = {'item' : orang, 'query' : 'Participant'}

    if request.method == 'POST':
        orang.delete()
        return redirect('/story6')

    return render(request, 'main/removePar.html', context)