from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.story6, name='story6'),
    path('addPerson/', views.addPerson, name='addPerson'),
    path('addEvent/', views.addEvent, name='addEvent'),
    path('updateEvent/<str:pk>/', views.updateEvent, name='updateEvent'),
    path('removeEvent/<str:pk>/', views.removeEvent, name='removeEvent'),
    path('removeParticipant/<str:pk>/', views.removeParticipant, name='removeParticipant'),
]
