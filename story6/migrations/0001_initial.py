# Generated by Django 3.1.2 on 2020-10-23 09:14

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Kegiatan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kegiatan', models.CharField(max_length=12, null=True)),
                ('note', models.CharField(max_length=25, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Orang',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=12, null=True)),
                ('kegiatan', models.ManyToManyField(to='story6.Kegiatan')),
            ],
        ),
    ]
