from django.forms import ModelForm
from .models import Kegiatan, Orang
from django.forms.widgets import CheckboxSelectMultiple

class KegiatanForm(ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        
        super(KegiatanForm, self).__init__(*args, **kwargs)
        
        self.fields["participants"].widget = CheckboxSelectMultiple()
        self.fields["participants"].queryset = Orang.objects.all()


class OrangForm(ModelForm):
    class Meta:
        model = Orang
        fields = '__all__'