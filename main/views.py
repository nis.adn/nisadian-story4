from django.shortcuts import render, redirect
from .models import *


def home(request):
    return render(request, 'main/home.html')

def story4(request):
    return render(request, 'main/story4.html')    

def message(request):
    return render(request, 'main/message.html')

def more(request):
    return render(request, 'main/more.html')  