from django.urls import path

from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.home, name='home'),
    path('story4/', views.story4, name='story4'),
    path('story4/message/', views.message, name='message'),
    path('story4/more/', views.more, name='more'),
]
