from django.test import TestCase, Client
from django.urls import resolve
from .views import *

# Create your tests here.

class Story7Test(TestCase):

    def test_url_story7_using_story7_func(self):
        response = Client().get('/story7/')
        found = resolve('/story7/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, story7)

    def test_story7_html_response(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'main/story7.html')

    def test_content_in_page_story7(self):
        response = Client().get('/story7/')
        html_response = response.content.decode('utf-8')
        self.assertIn("accordion", html_response)
        self.assertIn("script", html_response)
        self.assertIn("up", html_response)
        self.assertIn("down", html_response)

