
$(document).ready(function() {

    $('.checkbox-theme').click(function() {
        if(!$('.story7').hasClass('dark')) {
            // change to dark mode by adding 'dark' classes
            $('.story7').addClass('dark');
            $('.accordion-item').addClass('dark-acc');
            $('.copyright').addClass('dark-foot');
        } else {
            // back to light mode by removing 'dark' classes
            $('.story7').removeClass('dark');
            $('.accordion-item').removeClass('dark-acc');
            $('.copyright').removeClass('dark-foot');
        }
    })

    $('.accordion-item-header').click(function (){
        if(!$(this).next('.accordion-item-body').hasClass('active')) {
            // slidedown only the selected accordion when user clicked it
            $(this).addClass('active');
            $(this).parent().siblings().children('.accordion-item-header').removeClass('active');
            $(this).next('.accordion-item-body').slideDown('slow').addClass('active');
            $(this).parent().siblings().children('.accordion-item-body').removeClass('active').slideUp('slow');
        } else {
            // slideup to close the accordion
            $(this).removeClass('active');
            $(this).next('.accordion-item-body').slideUp('slow').removeClass('active');
        }
    });
    
    var selected = 0;                           // for storing index of the selected accordion
    var itemlist = $(".accordion");             // div class accordion yang paling luar
    var len=$(itemlist).children().length;      // total accordion-item

    $(".accordion .accordion-item .accordion-item-header .up").click(function() {
        // take index of the selected accordion, then set default style for the others
        selected = $(this).parent().parent().index()
        $(this).parent().next('.accordion-item-body').removeClass('active');
        $(this).parent().parent().siblings().children('.accordion-item-header').removeClass('active');

        // move up if the selected accordion is not the uppermost
        // eq untuk select accordion dengan index sebelum yang ingin dipindah, lalu lalu yg selected ditaruh di 'before'nya
        if(selected>0) {
            $(itemlist).children().eq(selected-1).before($(itemlist).children().eq(selected));
         	selected=selected-1;
        }
    });
    
    $(".accordion .accordion-item .accordion-item-header .down").click(function(){
        // take index of the selected accordion, then set default style for the others
        selected = $(this).parent().parent().index();
        $(this).parent().next('.accordion-item-body').removeClass('active');
        $(this).parent().parent().siblings().children('.accordion-item-header').removeClass('active');
        
        // move up if the selected accordion is not in the lowermost
        // eq untuk select accordion dengan index setelah yang ingin dipindah, lalu yg selected ditaruh di 'after'nya
        if(selected < len) {
            $(itemlist).children().eq(selected+1).after($(itemlist).children().eq(selected));
            selected=selected+1;
        }
   });

   $('.popup-gallery').magnificPopup({
       // for showing image with popup
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true,
        }
    });
});