from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from django.contrib.auth.models import User

# Create your tests here.

class Story9Test(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_story9_using_story9_func(self):
        response = self.client.get('/story9/')
        found = resolve('/story9/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, story9)

    def test_story9_html_response(self):
        response = self.client.get('/story9/')
        self.assertTemplateUsed(response, 'main/story9.html')

    def test_register_status_code_and_template(self):
        response = self.client.get('/story9/register/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/register.html')

    def test_if_register_url_when_logged_in(self):
        User.objects.create_user(
            username='nisa', 
            password='apaya'
        )
        self.client.login(username="nisa", password="apaya")
        response = self.client.get('/story9/register/')
        self.assertEqual(response.status_code, 302)

    def test_content_when_user_is_not_logged_in(self):
        response = self.client.get('/story9/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Authentication Required", html_response)

    def test_content_when_user_is_logged_in(self):
        User.objects.create_user(
            username='nisa', 
            password='apaya'
        )
        self.client.login(username="nisa", password="apaya")
        response = self.client.get('/story9/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Welcome,", html_response)
        self.assertIn("Logout", html_response)

    def test_register_success(self):
        data = {
            'username' : 'siapaya',
            'password1' : 'diadiadia',
            'password2' : 'diadiadia',
        }
        response = self.client.post('/story9/register/', data = data)
        self.assertEqual(response.status_code, 302)

    def test_log_in_success(self):
        User.objects.create_user(
            username='nisa', 
            password='apaya'
        )
        user_login = {
            'username' : 'nisa',
            'password' : 'apaya',
        }
        response = self.client.post('/story9/', user_login)
        self.assertEqual(response.status_code, 200)

    def test_log_in_error(self):
        User.objects.create_user(
            username='nisa', 
            password='apaya'
        )
        user_login = {
            'username' : 'nisa',
            'password' : 'bukanini',
        }
        response = self.client.post('/story9/', user_login)
        html_response = response.content.decode('utf-8')
        self.assertIn('Username or password is incorrect', html_response)

    def test_log_out(self):
        response = self.client.get('/story9/logout/')
        self.assertEqual(response.status_code, 302)

    def test_play_not_logged_in(self):
        response = self.client.get('/story9/play/')
        self.assertEqual(response.status_code, 302)

    def test_play_logged_in(self):
        User.objects.create_user(
            username='nisa', 
            password='apaya'
        )
        self.client.login(username="nisa", password="apaya")
        response = self.client.get('/story9/play/')
        self.assertEqual(response.status_code, 200)