from django.urls import path

from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.story9, name='story9'),
    path('logout/', views.story9Logout, name='story9Logout'),
    path('register/', views.register, name='register'),
    path('play/', views.play, name='play'),
]
