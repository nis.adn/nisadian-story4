from django.shortcuts import render, redirect
from django.contrib import messages
from django.utils.safestring import mark_safe
from django.contrib.auth import authenticate, login, logout
from .forms import RegisForm

# Create your views here.

def story9(request):
    context = {}

    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
        else:
            messages.info(request,'Username or password is incorrect')

    return render(request, 'main/story9.html', context)

def story9Logout(request):
    logout(request)
    return redirect('/story9')

def register(request):
    form = RegisForm()

    if request.user.is_authenticated:
        return redirect('/story9')

    if request.method == "POST":
        form = RegisForm(request.POST)
        if form.is_valid():
            form.save()
            user = authenticate(request, username=form.cleaned_data['username'], password=form.cleaned_data['password1'])
            login(request, user)
            return redirect('/story9')

    context = {'form' : form}
    return render(request, 'main/register.html', context)

def play(request):
    if not request.user.is_authenticated:
        return redirect('/story9')
    return render(request, 'main/play.html')