from django.shortcuts import render, redirect
from .models import *
from .forms import MatkulForm, CourseForm

def story5(request):
    matkuls = Matkul.objects.all()
    courses = Course.objects.all()

    context = {'matkuls' : matkuls, 'courses' : courses}

    return render(request, 'main/story5.html', context)

def course(request, pk_test):
    course = Course.objects.get(id=pk_test)
    context = {'course' : course}
    return render(request, 'main/course.html', context)

def addStatus(request):
    matkuls = Matkul.objects.all()
    form = MatkulForm()
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/story5')

    context ={'form' : form, 'matkuls' : matkuls, 'query':'add'}

    return render(request, 'main/addStatus.html', context)

def addCourse(request):
    courses = Course.objects.all()
    form = CourseForm()
    if request.method == 'POST':
        form = CourseForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/story5')

    context ={'form' : form, 'courses':courses, 'query':'add'}

    return render(request, 'main/addCourse.html', context)

def updateStatus(request, pk):
    matkuls = Matkul.objects.all()
    matkul = Matkul.objects.get(id=pk)
    form = MatkulForm(instance=matkul)

    if request.method == 'POST':
        form = MatkulForm(request.POST, instance=matkul)
        if form.is_valid():
            form.save()
            return redirect('/story5')

    context = {'form' : form, 'matkuls':matkuls, 'query':'update'}
    return render(request, 'main/addStatus.html', context)

def removeStatus(request, pk):
    matkuls = Matkul.objects.get(id=pk)
    context = {'item' : matkuls}

    if request.method == 'POST':
        matkuls.delete()
        return redirect('/story5')

    return render(request, 'main/remove.html', context)
    
def updateCourse(request, pk):
    courses = Course.objects.all()
    course = Course.objects.get(id=pk)
    form = CourseForm(instance=course)

    if request.method == 'POST':
        form = CourseForm(request.POST, instance=course)
        if form.is_valid():
            form.save()
            return redirect('/story5')

    context = {'form' : form, 'courses':courses, 'query':'update'}
    return render(request, 'main/addCourse.html', context)

def removeCourse(request, pk):
    courses = Course.objects.get(id=pk)
    context = {'item' : courses}

    if request.method == 'POST':
        courses.delete()
        return redirect('/story5')

    return render(request, 'main/delete.html', context)