# Generated by Django 3.1.2 on 2020-10-23 15:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story5', '0012_auto_20201023_2136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='year',
            field=models.CharField(choices=[('2020/2021', '2020/2021'), ('Other', 'Other'), ('2019/2020', '2019/2020')], max_length=25, null=True),
        ),
        migrations.AlterField(
            model_name='matkul',
            name='status',
            field=models.CharField(choices=[('Ongoing', 'Ongoing'), ('Done', 'Done'), ('Not Yet', 'Not Yet')], max_length=10, null=True),
        ),
    ]
