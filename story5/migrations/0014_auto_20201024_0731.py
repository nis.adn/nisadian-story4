# Generated by Django 3.1.2 on 2020-10-24 00:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story5', '0013_auto_20201023_2232'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='year',
            field=models.CharField(choices=[('2019/2020', '2019/2020'), ('Other', 'Other'), ('2020/2021', '2020/2021')], max_length=25, null=True),
        ),
        migrations.AlterField(
            model_name='matkul',
            name='status',
            field=models.CharField(choices=[('Not Yet', 'Not Yet'), ('Ongoing', 'Ongoing'), ('Done', 'Done')], max_length=10, null=True),
        ),
    ]
