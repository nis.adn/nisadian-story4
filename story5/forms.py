from django.forms import ModelForm
from .models import Matkul, Course

class MatkulForm(ModelForm):
    class Meta:
        model = Matkul
        fields = '__all__'

class CourseForm(ModelForm):
    class Meta:
        model = Course
        fields = '__all__'