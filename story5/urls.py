from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.story5, name='story5'),
    path('addStatus/', views.addStatus, name='addStatus'),
    path('addCourse/', views.addCourse, name='addCourse'),
    path('course/<str:pk_test>/', views.course, name='course'),
    path('updateStatus/<str:pk>/', views.updateStatus, name='updateStatus'),
    path('removeStatus/<str:pk>/', views.removeStatus, name='removeStatus'),
    path('updateCourse/<str:pk>/', views.updateCourse, name='updateCourse'),
    path('removeCourse/<str:pk>/', views.removeCourse, name='removeCourse'),
]
