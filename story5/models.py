from django.db import models

# Create your models here.

class Course(models.Model) :
    name = models.CharField(max_length=12, null=True)
    sks = models.DecimalField(max_digits=1, decimal_places=0)
    semester = models.DecimalField(max_digits=1, decimal_places=0, null=True)
    ta = {('2019/2020', '2019/2020'), ('2020/2021', '2020/2021'), ('Other', 'Other')}
    year = models.CharField(max_length=25, null=True, choices=ta)
    note = models.CharField(max_length=25, null=True)
    
    def __str__(self):
        return self.name

class Matkul(models.Model):
    stat = {('Done', 'Done'), ('Not Yet', 'Not Yet'), ('Ongoing', 'Ongoing')}

    course = models.ForeignKey(Course, null=True, on_delete=models.CASCADE)
    status = models.CharField(max_length=10, null=True, choices=stat)
    
    def __str__(self):
        return self.course.name